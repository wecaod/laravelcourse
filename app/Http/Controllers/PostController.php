<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        DB::select('select * from `posts` order by `created_at` desc');
         $posts=  new Post();
         if($request->filled("cat_id") && $request->cat_id){
             $posts=$posts->where('cat_id',$request->cat_id);
         }

         if($request->filled("title") && $request->title){
             $posts=$posts->where('title','like','%'.$request->title.'%');
         }

        $posts=$posts->orderBy('created_at','desc')->paginate(5);
        $categories=Category::get();
        $data=[
            'posts'=>$posts,
            'categories'=>$categories,
        ];
       return view('admin.post.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories=Category::get();

        return view('admin.post.create',compact('categories'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'cat_id'=>'required',
            'image'=>'required|image|max:2048',
        ]);
        $imageName=null;
        if($request->hasFile('image')){
//            getClientOriginalExtension
             $imageName=date('Y-m-d H-m-i').rand(100,1000).'.'.$request->image->extension();
             $request->image->move(public_path('uploads'),$imageName);
        }
        $post=new post();
        $post->title=$request->title;
        $post->description=$request->description;
        $post->cat_id=$request->cat_id;
        $post->main_image=$imageName;
        $post->save();

        return redirect()->route('posts.index')->with(
            ['operation_msg'=>'تمت العملية بنجاح','success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit( $post)
    {
        $categories=Category::get();
        $post= Post::findOrFail($post);

        $data['post']=$post;
        $data['categories']=$categories;
     return view('admin.post.update',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title=$request->title;
        $post->description=$request->description;
        $post->cat_id=$request->cat_id;
        $post->save();
        return redirect()->route('posts.index')->with('operation_msg','تم تحديث البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $image=$post->main_image;
        if($post->delete()){
            unlink(public_path('uploads/'.$image));
        }
//        return response()->json(["message"=>'تم حذف الخبر بنجاح']);
        return redirect()->route('posts.index')->with('operation_msg','تم حذف الخبر بنجاح');
    }
}
