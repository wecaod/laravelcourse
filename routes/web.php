<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;


Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::group(['prefix'=>'system','middleware'=>'auth:web'],function(){

    Route::get('/',function (){
        return view('admin.layouts.index');
    });

    Route::group(['prefix'=>'posts'],function(){
        Route::get('/',[PostController::class,'index'])->name('posts.index');
        Route::get('/create',[PostController::class,'create'])->name('post.create');
        Route::post('/store',[PostController::class,'store'])->name('post.store');
        Route::get('/{post}/edit',[PostController::class,'edit'])->name('post.edit');
        Route::post('/{post}/update',[PostController::class,'update'])->name('post.update');
        Route::post('/{post}/delete',[PostController::class,'destroy'])->name('post.delete');
    });
});

//Route::get('/getName',[UserController::class,'getMyName'])->name('getName');
//Route::post('/getName',[UserController::class,'postName'])->name('post.name');


//Route::resource('post',PostController::class)->except(['destroy']);

//
//
//Route::view('/about-us','about_us',['name'=>"my name is : abdallah"]);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home.home');
