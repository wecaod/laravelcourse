@extends('admin.layouts.index')

@section('footer')
    تعديل خبر
@endsection
@section('content')

    <div class="row">

        <div class="col-md-6 col-xs-12">

            <form action="{{route('post.update',$post->id)}}" method="post"  style="direction: rtl !important;">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1"> عنوان الخبر</label>
                    <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="ادخل عنوان الخبر"
                        value="{{$post->title}}">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1"> تصنيف الخبر</label>
                    <select name="cat_id" class="form-control">
                        <option value="">اختر التصنيف</option>

                                                @foreach($categories as $cat)
                                                    <option value="{{$cat->id}}" {{$post->cat_id ==$cat->id?'selected':''}}>
                                                        {{$cat->name}}
                                                    </option>
                                                @endforeach

{{--                        @foreach($categories as $cat)--}}
{{--                            <option value="{{$cat->id}}" @if($post->cat_id ==$cat->id ) selected @endif>--}}
{{--                                {{$cat->name}}--}}
{{--                            </option>--}}
{{--                        @endforeach--}}
                    </select>
                    @error('cat_id')
                    <div class="red" style="color: red"">{{ $message }}</div>
                        @enderror
                    </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1"> تفاصيل الخبر</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"
                              placeholder="تفاصيل الخبر">{{$post->description}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" >تحديث </button>
                </div>
            </form>
        </div>
        </div>
@endsection

