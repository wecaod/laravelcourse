@extends('admin.layouts.index')

@section('footer')
      إضافة خبر جديد
@endsection
@section('content')
{{--        <div class="row justify-center">--}}
{{--            <div class="col-md-6">--}}
{{--                @if ($errors->any())--}}
{{--                    <div class="alert alert-danger">--}}
{{--                        <ul>--}}
{{--                            @foreach ($errors->all() as $error)--}}
{{--                                <li>{{ $error }}</li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}



    <div class="row justify-center">

        <div class="col-md-6 col-xs-12">

            <form action="{{route('post.store')}}" method="post"  style="direction: rtl !important;" enctype="multipart/form-data">
                @csrf


            <div class="form-group">
                        <label for="exampleFormControlInput1"> صورة الخبر</label>
                        <input type="file" name="image" class="form-control" accept="image/*">
                        @error('image')
                        <div class="red" style="color: red"">{{ $message }}</div>
                @enderror
            </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1"> عنوان الخبر</label>
                    <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="ادخل عنوان الخبر">
                    @error('title')
                    <div class="red" style="color: red"">{{ $message }}</div>
                    @enderror
                </div>
         <div class="form-group">
            <label for="exampleFormControlInput1"> تصنيف الخبر</label>
                <select name="cat_id" class="form-control">
                    @foreach($categories as $cat)
                            <option value="{{$cat->id}}">
                                {{$cat->name}}
                            </option>
                    @endforeach
                </select>
            @error('cat_id')
            <div class="red" style="color: red"">{{ $message }}</div>
        @enderror
    </div>


                <div class="form-group">
                    <label for="exampleFormControlTextarea1"> تفاصيل الخبر</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"
                              placeholder="تفاصيل الخبر"></textarea>
                    @error('description')
                    <div class="red" style="color: red"">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" >حفظ</button>
                </div>
            </form>
        </div>

    </div>

@endsection
