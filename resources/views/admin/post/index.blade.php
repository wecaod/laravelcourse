@extends('admin.layouts.index')

@section('content')
    <div class="row" dir="rtl">
        <div class="col-md-6 col-offset-3">


            <form action="{{route('posts.index')}}" method="get">



                <div class="form-group">

                        <input type="text" name="title" class="form-control" placeholder="بحث"/>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1"> تصنيف الخبر</label>
                    <select name="cat_id" class="form-control">
                        <option value="">اختر التصنيف</option>
                        @foreach($categories as $cat)
                            <option value="{{$cat->id}}">
                                {{$cat->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">فلترة</button>
                    <a href="{{route('posts.index')}}" class="btn btn-secondary">مسح</a>
                </div>
            </form>
        </div>
    </div>
<div class="row">
    <div class="col-md-12">

            <button class="btn btn-primary mt-4 " onclick="location.href='{{route('post.create')}}'">
                اضافة خبر جديد
            </button>
    </div>
</div>
<div class="row ">
    <div class="col-md-12">




    <div class="col-md-8 col-xs-12">
        @if(\Session::has('operation_msg'))
            <label class="alert alert-success col-md-12">

                    {{ \Illuminate\Support\Facades\Session::get('operation_msg') }}

            </label>
        @endif


            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Table striped</h5>
             <table class="mb-0 table table-striped">
        <thead class="thead-dark red" >
        <tr>
            <th>#</th>
            <th>عنوان الخبر </th>
            <th>صورة الخبر </th>
            <th>تصنيف الخبر </th>
            <th>تفاصيل  الخبر </th>
            <th>العمليات </th>


        </tr>
        </thead>
        <tbody>
        @foreach($posts as $key=>$post)

        <tr>
{{--                <td>{{$post->id}}</td>--}}

                <td>  {{ $posts->firstItem() + $key }}</td>
                <td>{{$post->title}}</td>
                <td><img src="{{'/uploads/'.$post->main_image}}" width="200" height="200"></td>
                <td>{{@$post->category->name}}</td>
                <td>{{$post->description}}</td>
                <td>
                    <a class="btn btn-sm btn-secondary" href="{{route('post.edit',['post'=>$post->id])}}" target="_self">
                        <i class="bi bi-pencil-square"></i>
                        تعديل
                    </a>
                    <a class="btn btn-sm btn-danger" href="javascript:;" target="_self" onclick="document.getElementById('delete_news_{{$post->id}}').submit()">
                        <i class="bi bi-pencil-square"></i>
                        حذف
                    </a>
                    <form action="{{route('post.delete',['post'=>$post->id])}}"    method="post" id="delete_news_{{$post->id}}">


                        @csrf
                    </form>
                </td>
        </tr>

        @endforeach

        </tbody>
    </table>

        <div class="row justify-center">
            {{$posts->links()}}

        </div>
        </div>
        </div>
    </div>
    </div>
    </div>
@endsection
